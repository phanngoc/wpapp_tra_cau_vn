﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2
{
    public class Helper
    {
        
        public static Helper instance = null;

        public static Helper getInstance()
        {
            if(instance!=null)
            {
                return instance;
            }
            else
            {
                instance = new Helper();
                return instance;
            }
        }
        public Helper()
        {

        }
        public static void report()
        {

        }
        public  void write(String trace,String text)
        {
            System.Diagnostics.Debug.WriteLine(trace + " : " + text);
        }
        public void write(Object obj,String trace, String text)
        {
            System.Diagnostics.Debug.WriteLine(obj.ToString()+"@"+trace + " : " + text);
        }
        public  void write(String text)
        {
            System.Diagnostics.Debug.WriteLine("Global : " + text);
        }
    }
    public class Trace
    {
        public int numberline { set;get; }
        public bool isPassed { set; get; }

        public String description { set; get; }
        public String method { set; get; }
        public String classname { set; get; }
        public Trace(int numberline,String des,String cate)
        {
            this.numberline = numberline;
            this.description = des;
            this.method = method;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2.Model
{
    [Table(Name = "Sentence")]
    public class SentenceFa : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _sentence_id;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get
            {
                return _sentence_id;
            }

            set
            {
                if (_sentence_id != value)
                {
                    NotifyPropertyChanging("WordId");
                    _sentence_id = value;
                    NotifyPropertyChanged("WordId");
                }
            }
        }

        private string _en;
        [Column(DbType = "nvarchar(255)", CanBeNull = false)]
        public string En
        {
            get
            {
                return _en;
            }

            set
            {
                if (_en != value)
                {
                    NotifyPropertyChanging("En");
                    _en = value;
                    NotifyPropertyChanged("En");
                }
            }
        }

        private string _vi;
        [Column(DbType = "nvarchar(255)", CanBeNull = false)]
        public string Vi
        {
            get
            {
                return _vi;
            }

            set
            {
                if (_vi != value)
                {
                    NotifyPropertyChanging("Vi");
                    _vi = value;
                    NotifyPropertyChanged("Vi");
                }
            }
        }

        private EntityRef<Favourite> _favorite;
        [Association(Name = "FK_Fa_Sentence", Storage = "_favorite", ThisKey = "FavouriteId",
        OtherKey = "Id", IsForeignKey = true)]
        public Favourite Favourite
        {
            get
            {
                return _favorite.Entity;
            }
            set
            {
                NotifyPropertyChanging("Favourite");
                _favorite.Entity = value;

                if (value != null)
                {
                    _favourite_id = value.Id;
                }

                NotifyPropertyChanged("Favourite");
            }
        }

        public int _favourite_id;
        [Column(CanBeNull = false)]
        public int FavouriteId
        {
            get
            {
                return _favourite_id;
            }
            set
            {
                if (_favourite_id != value)
                {
                    NotifyPropertyChanging("FavouriteId");
                    _favourite_id = value;
                    NotifyPropertyChanged("FavouriteId");
                }
            }
        }



        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2.Model
{
    [Table(Name = "WordSearch")]
    public class WordSearch : INotifyPropertyChanged, INotifyPropertyChanging
    {
        private int _word_id;
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int WordId
        {
            get
            {
                return _word_id;
            }

            set
            {
                if (_word_id != value)
                {
                    NotifyPropertyChanging("WordId");
                    _word_id = value;
                    NotifyPropertyChanged("WordId");
                }
            }
        }

        private string _wordtext;
        [Column(DbType = "nvarchar(255)", CanBeNull = false)]
        public string WordName
        {
            get
            {
                return _wordtext;
            }

            set
            {
                if (_wordtext != value)
                {
                    NotifyPropertyChanging("WordName");
                    _wordtext = value;
                    NotifyPropertyChanged("WordName");
                }
            }
        }

        private EntityRef<History> _history;
        [Association(Name = "FK_History_Word", Storage = "_history", ThisKey = "HistoryId",
        OtherKey = "Id", IsForeignKey = true)]
        public History MHistory
        {
            get
            {
                return _history.Entity;
            }
            set
            {
                NotifyPropertyChanging("MHistory");
                _history.Entity = value;

                if (value != null)
                {
                    _history_id = value.Id;
                }

                NotifyPropertyChanged("MHistory");
            }
        }

        public int _history_id;
        [Column(CanBeNull = false)]
        public int HistoryId
        {
            get
            {
                return _history_id;
            }
            set
            {
                if (_history_id != value)
                {
                    NotifyPropertyChanging("HistoryId");
                    _history_id = value;
                    NotifyPropertyChanged("HistoryId");
                }
            }
        }



        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2.Model
{
    [Table(Name = "History")]
    public class History : INotifyPropertyChanged, INotifyPropertyChanging
    {
        public History()
        {

            _wordsearchs = new EntitySet<WordSearch>(
                    course =>
                    {
                        NotifyPropertyChanging("WordSearch");
                        course.MHistory = this;
                    },
                    course =>
                    {
                        NotifyPropertyChanging("WordSearch");
                        course.MHistory = null;
                    }
                );
        }

        private int _id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    NotifyPropertyChanging("Id");
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }



        private string _date;

        [Column(DbType = "nvarchar(255)", CanBeNull = false)]
        public string Date
        {
            get
            {
                return _date;
            }
            set
            {
                if (_date != value)
                {
                    NotifyPropertyChanging("Date");
                    _date = value;
                    NotifyPropertyChanged("Date");
                }
            }
        }

        private EntitySet<WordSearch> _wordsearchs;

        [Association(Name = "FK_History_Word", Storage = "_wordsearchs", ThisKey = "Id", OtherKey = "HistoryId")]
        public EntitySet<WordSearch> WordSearch
        {
            get
            {
                return _wordsearchs;
            }
            set
            {
                _wordsearchs.Assign(value);
            }
        }


        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}


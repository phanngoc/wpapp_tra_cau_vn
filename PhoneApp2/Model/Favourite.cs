﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2.Model
{
    [Table(Name = "Favourite")]
    public class Favourite : INotifyPropertyChanged, INotifyPropertyChanging
    {
        public Favourite()
        {

            _sentences = new EntitySet<SentenceFa>(
                    sentence =>
                    {
                        NotifyPropertyChanging("Sentences");
                        sentence.Favourite = this;
                    },
                    sentence =>
                    {
                        NotifyPropertyChanging("Sentences");
                        sentence.Favourite = null;
                    }
                );
        }

        private int _id;

        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (_id != value)
                {
                    NotifyPropertyChanging("Id");
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }



        private string _title;

        [Column(DbType = "nvarchar(255)", CanBeNull = false)]
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                if (_title != value)
                {
                    NotifyPropertyChanging("Title");
                    _title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        private EntitySet<SentenceFa> _sentences;

        [Association(Name = "FK_Fa_Sentence", Storage = "_sentences", ThisKey = "Id", OtherKey = "FavouriteId")]
        public EntitySet<SentenceFa> Sentences
        {
            get
            {
                return _sentences;
            }
            set
            {
                _sentences.Assign(value);
            }
        }


        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify the data context that a data context property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify the page that a data context property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}


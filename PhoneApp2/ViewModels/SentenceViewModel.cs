﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using PhoneApp2;
using PhoneApp2.Model;
namespace PhoneApp2.ViewModels
{
    public class SentenceViewMode : INotifyPropertyChanged
    {
        private MeDataContext context;
        public ObservableCollection<SentenceFa> Sentence { get; private set; }
        public bool IsDataLoaded { get; private set; }

        public SentenceViewMode()
        {
            this.Sentence = new ObservableCollection<SentenceFa>();
            context = new MeDataContext(MeDataContext.DBConnectionString);
            context.SubmitChanges();
        }

        public void LoadHistoryData()
        {
            if (context.History.Count() > 0)
            {
                List<SentenceFa> sentencelist = context.Sentence.ToList();
                Sentence = new ObservableCollection<SentenceFa>(sentencelist);
            }

            IsDataLoaded = true;
        }

        public void InsertNewSentence(ResultSearch fa)
        {
            NewSentence = new SentenceFa();
            NewSentence.En = fa.En;
            NewSentence.Vi = fa.Vi;
            context.Sentence.InsertOnSubmit(NewSentence);
            context.SubmitChanges();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private SentenceFa _sentence;
        public SentenceFa NewSentence
        {
            get
            {
                return _sentence;
            }
            set
            {
                _sentence = value;
                NotifyPropertyChanged("NewSentence");
            }
        }
    }
}

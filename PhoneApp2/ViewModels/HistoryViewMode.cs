﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using PhoneApp2;
using PhoneApp2.Model;
using System.Data.Common;
namespace PhoneApp2.ViewModels
{
    public class HistoryViewMode : INotifyPropertyChanged
    {
        private MeDataContext context;
        public ObservableCollection<History> History { get; private set; }
        public ObservableCollection<SentenceFa> Sentence { get; private set; }

        public List<WordHistory> wordHistory { get; set; }
        public bool IsDataLoaded { get; private set; }

        public HistoryViewMode()
        {
            this.History = new ObservableCollection<History>();
            this.Sentence = new ObservableCollection<SentenceFa>();

            context = new MeDataContext(MeDataContext.DBConnectionString);

            if (!context.DatabaseExists())
            {
                context.CreateDatabase();
                context.SubmitChanges();
            }
            AddNewFavourite("Những câu yêu thích");
            LoadSentenceData();
            LoadWordHistory();
        }

        public void AddNewText(String text)
        {
            WordSearch word = new WordSearch();
            word.WordName = text;
            word.HistoryId = InsertNewHistory();
            context.WordSearch.InsertOnSubmit(word);
            context.SubmitChanges();
            LoadWordHistory();
        }

        public void LoadWordHistory()
        {
            wordHistory = new List<WordHistory>();
            List<History> History = context.History.ToList();
            History.Reverse();
            foreach (History his in History)
            {
                List<WordSearch> wordlist = context.WordSearch
                .Where(n => n.HistoryId == his.Id)
                .ToList();
                wordlist.Reverse();
                wordHistory.Add(new WordHistory(his.Date, wordlist));
            }
        }

        public void AddNewFavourite(String fa)
        {
            if(context.Favourite.ToList().Count() == 0)
            {
                Favourite favou = new Favourite();
                favou.Title = fa;
                context.Favourite.InsertOnSubmit(favou);
                context.SubmitChanges();
            }         
        }

        public void LoadSentenceData()
        {
                Helper.getInstance().write(this, "LoadSentenceData", "so cau luu duoc la:" + context.Sentence.Count());
                List<SentenceFa> sentencelist = context.Sentence.ToList();
                sentencelist.Reverse();
                Sentence = new ObservableCollection<SentenceFa>(sentencelist);

            IsDataLoaded = true;
        }
        public int InsertNewHistory()
        {
            string fullDate;
            string date = DateTime.Today.Day.ToString();
            string month = DateTime.Today.Month.ToString();
            string year = DateTime.Today.Year.ToString();

            fullDate = month + "/" + date + "/" + year;

            int count = this.context.History.Where(n => n.Date == fullDate).Count();
            if(count == 0)
            {
                NewHistory = new History();
                NewHistory.Date = fullDate;
                context.History.InsertOnSubmit(NewHistory);
                context.SubmitChanges();

                return NewHistory.Id;
            }
            else
            {
                return this.context.History.Where(n => n.Date == fullDate).FirstOrDefault().Id;
            }
            
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private History _history;
        public History NewHistory
        {
            get
            {
                return _history;
            }
            set
            {
                _history = value;
                NotifyPropertyChanged("History");
            }
        }
        private SentenceFa _sentence;
        public SentenceFa NewSentence
        {
            get
            {
                return _sentence;
            }
            set
            {
                _sentence = value;
                NotifyPropertyChanged("NewSentence");
            }
        }
        public void AddNewSentence(String Vi,String En)
        {
            NewSentence = new SentenceFa();
            NewSentence.Vi = Vi;
            NewSentence.En = En;
            NewSentence.FavouriteId = 1;
            context.Sentence.InsertOnSubmit(NewSentence);
            context.SubmitChanges();
            LoadSentenceData();
        }
        public void RemoveSentence(string text)
        {
            List<SentenceFa> list = context.Sentence
                .Where(n => n.En == text)
                .ToList();
            if (list.Count() > 0)
            {
                context.Sentence.DeleteOnSubmit(list.FirstOrDefault());
                context.SubmitChanges();
            }
            LoadSentenceData();
        }
        public void DeleteAllHistory()
        {

            var query1 = from r1 in context.WordSearch select r1;
            context.WordSearch.DeleteAllOnSubmit(query1);
            context.SubmitChanges();
            Helper.getInstance().write(this,"DeleteAllHistory", "number word search :" + context.WordSearch.Count());
            
              var query = from r in context.History select r;
              context.History.DeleteAllOnSubmit(query);
              context.SubmitChanges();

            Helper.getInstance().write(this,"DeleteAllHistory", "number history" + context.History.Count());
            LoadWordHistory();
        }
    }
}

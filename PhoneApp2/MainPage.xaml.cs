﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.IO;
using Newtonsoft.Json.Linq;
using PhoneApp2.Resources;
using PhoneApp2.ViewModels;
using System.Collections.ObjectModel;
using PhoneApp2.Model;
using Windows.Phone.Speech.Synthesis;
using Microsoft.Phone.Net.NetworkInformation;

namespace PhoneApp2
{
    public partial class MainPage : PhoneApplicationPage
    {
        public ObservableCollection<ResultSearch> resultSearch { get; set; }
       
        // Constructor
        public MainPage()
        {
            System.Diagnostics.Debug.WriteLine("Init");
            InitializeComponent();
            DataContext = App.ViewModel;
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            resultSearch = new ObservableCollection<ResultSearch>();
       


            listResult.ItemsSource = resultSearch;
            listHistory.ItemsSource = App.ViewModel.wordHistory;

            StreamReader sr = new StreamReader("Assets\\about.html");
            string str = sr.ReadToEnd();
            //AboutBrowser.NavigateToString(str);
            //cddfddfd
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("OnNavigatedTo");
     
        }
        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
        private void Search(object sender, RoutedEventArgs e)
        {
           // App.ViewModel.LoadHistoryData();
           // llsStudents.ItemsSource = App.ViewModel.History;
            actionSearch();
        }
        private void actionSearch()
        {
            if (CheckNetworkConnection())
            {
                if (textSearch.Text != "")
                {
                    progressBar1.Visibility = Visibility.Visible;
                    App.ViewModel.AddNewText(textSearch.Text);
                    listHistory.ItemsSource = App.ViewModel.wordHistory;
                    System.Diagnostics.Debug.WriteLine("Day ne");
                    string avatarUri = "http://tracau.vn:1981/WBBcwnwQpV89/" + textSearch.Text + "/en/JSON_CALLBACK";

                    HttpWebRequest WebReq =
                      (HttpWebRequest)WebRequest.Create(avatarUri);
                    WebReq.Method = "GET";
                    WebReq.BeginGetResponse(new AsyncCallback(getWord), WebReq);
                }
                else
                {
                    MessageBox.Show("You must type the word");
                }
            }
            else
            {
                MessageBox.Show("No connection internet");
            }
        }

        public static bool CheckNetworkConnection()
        {
            var networkInterface = NetworkInterface.NetworkInterfaceType;

            bool isConnected = false;
            if ((networkInterface == NetworkInterfaceType.Wireless80211) || (networkInterface == NetworkInterfaceType.MobileBroadbandGsm) || (networkInterface == NetworkInterfaceType.MobileBroadbandCdma))
                isConnected = true;

            else if (networkInterface == NetworkInterfaceType.None)
                isConnected = false;
            return isConnected;
        }



        private void getWord(IAsyncResult result)
        {
            HttpWebRequest webRequest = (HttpWebRequest)result.AsyncState;
            var httpResponse = (HttpWebResponse)webRequest.EndGetResponse(result);
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                string responseText = streamReader.ReadToEnd();         
                processJson(responseText);
                streamReader.Close();
            }
        }
        private void processJson(string result)
        {
            string json = result.Substring(14, result.Length - 16);
          //  System.Diagnostics.Debug.WriteLine(json);
            JObject jsonObject = JObject.Parse(json);
            JArray jsonArray = (JArray)jsonObject["sentences"];
            ObservableCollection<ResultSearch> temp ;
            temp = new ObservableCollection<ResultSearch>();
            for( int i=0 ;i<jsonArray.Count;i++)
            {
              string en = (String)jsonArray[i]["fields"]["en"];
              string vi = (String)jsonArray[i]["fields"]["vi"];
              temp.Add(new ResultSearch(vi, en));
            }
            UIThread.Invoke(() => { listResult.ItemsSource = temp; progressBar1.Visibility = Visibility.Collapsed; });       
        }

        
        private void listWord_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listResult.SelectedItem == null)
                return;
           
        }

        private void listFavourite_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Helper.getInstance().write(this,"listFavourite_SelectionChanged","listFavourite_SelectionChanged");
      
        }
         private void addButton_Click_1(object sender, RoutedEventArgs e)
         {
             popupAddFavourite.IsOpen = false;
             App.ViewModel.AddNewSentence((listResult.SelectedItem as ResultSearch).Vi, (listResult.SelectedItem as ResultSearch).En);
             listFavourite.ItemsSource = App.ViewModel.Sentence;
         }
       
         private void cancelButton_Click_1(object sender, RoutedEventArgs e)
         {
             popupAddFavourite.IsOpen = false;
         }
         private void DeleteFavourite_Click(object sender, RoutedEventArgs e)
         {
             string text = ((((sender as Button).Parent as Grid).Children[0] as StackPanel).Children[0] as TextBlock).Text;
             Helper.getInstance().write(this,"DeleteFavourite_Click","deleteFavourite_Click " + (listFavourite.SelectedItem == null));

                 App.ViewModel.RemoveSentence(text);
                 listFavourite.ItemsSource = App.ViewModel.Sentence;


         }

         private void WordHistory_Tap(object sender, System.Windows.Input.GestureEventArgs e)
         {
             tracauPivot.SelectedItem = SearchWord;
             textSearch.Text = ((sender as StackPanel).Children[0] as TextBlock).Text;
             Helper.getInstance().write(this,"WordHistory_Tap","Text la " + textSearch.Text);
             actionSearch();
         }
         private void Pronuncation_Click(object sender, RoutedEventArgs e)
         {
            //string text =  (((sender as Button).Parent as StackPanel).Children[0] as TextBlock).Text;
             string text = (((((sender as Button).Parent as Grid).Parent as Grid).Children[0] as StackPanel).Children[0] as TextBlock).Text;
            // string text = (listResult.SelectedItem as ResultSearch).En;
            Helper.getInstance().write(this,"Pronuncation_Click","Text phat am la " + textSearch.Text);
            Speak(text);
         }
         private async void Speak(String text)
         {
             SpeechSynthesizer synth = new SpeechSynthesizer();
             await synth.SpeakTextAsync(text);
         }
         private void addButton_ShowPopup(object sender, RoutedEventArgs e)
         {
             popupAddFavourite.IsOpen = true;
         }

         private void tracauPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             if (tracauPivot.SelectedIndex == 2)
             {
                 this.ApplicationBar.IsVisible = true;
             }
             else
             {
                 this.ApplicationBar.IsVisible = false;
             }
         }

         private void ApplicationBarIconButton_Click(object sender, EventArgs e)
         {
             MessageBoxResult mbr = MessageBox.Show("Are you sure you want to delete all history?", "Warning", MessageBoxButton.OKCancel);

             if (mbr == MessageBoxResult.OK)
             {
                 App.ViewModel.DeleteAllHistory();
                 listHistory.ItemsSource = App.ViewModel.wordHistory;
             }
             else
             {
                 // Cancel pressed
             }
         }

        

    }
       
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.ComponentModel;
using PhoneApp2.Model;
namespace PhoneApp2
{
    public class WordHistory : INotifyPropertyChanged
    {
        private bool isExpanded = false;
        public String Date { get; set; }
        public WordHistory(String history ,List<WordSearch> wordtext)
        {
            this.Date = history;
            this.Options = wordtext;
        }
        public IList<WordSearch> Options
        {
            get;
            set;
        }

        public bool HasNoOptions
        {
            get
            {
                return this.Options == null || this.Options.Count == 0;
            }
        }

        public bool IsExpanded
        {
            get
            {
                return this.isExpanded;
            }
            set
            {
                if (this.isExpanded != value)
                {
                    this.isExpanded = value;
                    this.OnPropertyChanged("IsExpanded");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneApp2
{
    public class ResultSearch
    {
        public String Vi
        {
            get;
            set;
        }
        public String En
        {
            get;
            set;
        }
        public ResultSearch(String Vi,String En)
        {
            this.Vi = Vi;
            this.En = En;
        }
    }
}

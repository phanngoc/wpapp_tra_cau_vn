﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneApp2.Model;
namespace PhoneApp2
{
    public class MeDataContext : DataContext
    {
        // Specify the connection string as a static, used in main page and app.xaml.
        public static string DBConnectionString = "Data Source=isostore:/Tracau.sdf";

        // Pass the connection string to the base class.
        public MeDataContext(string connectionString)
            : base(connectionString)
        {
            this.Favourite = this.GetTable<Favourite>();
            this.History = this.GetTable<History>();
            this.Sentence = this.GetTable<SentenceFa>();
            this.WordSearch = this.GetTable<WordSearch>();
        }

        // Specify a single table for the to-do items.
        public Table<Favourite> Favourite;
        public Table<History> History;
        public Table<SentenceFa> Sentence;
        public Table<WordSearch> WordSearch;
    }
}

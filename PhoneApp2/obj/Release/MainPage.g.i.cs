﻿#pragma checksum "D:\study\windowphone\PhoneApp2\PhoneApp2\MainPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3E9FF6DD5FEAC4D591AA290ED5ABEFF6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace PhoneApp2 {
    
    
    public partial class MainPage : Microsoft.Phone.Controls.PhoneApplicationPage {
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid ContentPanel;
        
        internal Microsoft.Phone.Controls.Pivot tracauPivot;
        
        internal Microsoft.Phone.Controls.PivotItem SearchWord;
        
        internal System.Windows.Controls.Grid contentTracau;
        
        internal System.Windows.Controls.TextBox textSearch;
        
        internal Microsoft.Phone.Controls.LongListSelector listResult;
        
        internal Microsoft.Phone.Controls.PivotItem Favourite;
        
        internal Microsoft.Phone.Controls.LongListSelector listFavourite;
        
        internal Microsoft.Phone.Controls.PivotItem History;
        
        internal System.Windows.Controls.ListBox listHistory;
        
        internal Microsoft.Phone.Controls.PivotItem About;
        
        internal System.Windows.Controls.Primitives.Popup popupAddFavourite;
        
        internal System.Windows.Controls.Button addFavouriteButton;
        
        internal System.Windows.Controls.Button cancelButton;
        
        internal Microsoft.Phone.Shell.ApplicationBar appbar1;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/PhoneApp2;component/MainPage.xaml", System.UriKind.Relative));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.ContentPanel = ((System.Windows.Controls.Grid)(this.FindName("ContentPanel")));
            this.tracauPivot = ((Microsoft.Phone.Controls.Pivot)(this.FindName("tracauPivot")));
            this.SearchWord = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("SearchWord")));
            this.contentTracau = ((System.Windows.Controls.Grid)(this.FindName("contentTracau")));
            this.textSearch = ((System.Windows.Controls.TextBox)(this.FindName("textSearch")));
            this.listResult = ((Microsoft.Phone.Controls.LongListSelector)(this.FindName("listResult")));
            this.Favourite = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("Favourite")));
            this.listFavourite = ((Microsoft.Phone.Controls.LongListSelector)(this.FindName("listFavourite")));
            this.History = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("History")));
            this.listHistory = ((System.Windows.Controls.ListBox)(this.FindName("listHistory")));
            this.About = ((Microsoft.Phone.Controls.PivotItem)(this.FindName("About")));
            this.popupAddFavourite = ((System.Windows.Controls.Primitives.Popup)(this.FindName("popupAddFavourite")));
            this.addFavouriteButton = ((System.Windows.Controls.Button)(this.FindName("addFavouriteButton")));
            this.cancelButton = ((System.Windows.Controls.Button)(this.FindName("cancelButton")));
            this.appbar1 = ((Microsoft.Phone.Shell.ApplicationBar)(this.FindName("appbar1")));
        }
    }
}

